<?php

	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	$data = json_decode(file_get_contents("php://input"));
	$met = $_SERVER['REQUEST_METHOD'];
	$uri = $_SERVER['REQUEST_URI'];

	switch($met){
		case 'POST':
			if(!empty($data->id) && !empty($data->name) && !empty($data->description) && !empty($data->price)){
				http_response_code(201);
				$conn = connect();
				$sql = "INSERT INTO books (id, name, description, price, eliminado) VALUES  ('".$data->id."','".$data->name."','".$data->description."',".$data->price.",0)";

				if ($conn->query($sql) === TRUE) {
					$message="New record created successfully";
				} else {
					$message="Error: " . $sql . ", " . $conn->error;
				}

				$conn->close();


				echo json_encode(array(
					"method:" => $met,
					"message" => $message,
					"data-receive" => array(
					  "id" => $data->id,
					  "name" => $data->name,
					  "description" => $data->description,
					  "price" => $data->price
					)
				  )
				);
			}
			else{
				http_response_code(503);
				echo json_encode(array("message" => "No data was received"));
			}
		break;
		
		case 'PUT':
			if(!empty($data->id)){
				http_response_code(201);
				$conn = connect();
				$sql = "UPDATE books SET ";
				
				if(!empty($data->name)){
					$sql = $sql." name='".$data->name."', "; 
				}
				if(!empty($data->description)){
					$sql = $sql."description='".$data->description."', "; 
				}
				if(!empty($data->price)){
					$sql = $sql."price='".$data->price."', "; 
				}
				if(!empty($data->eliminado)){
					$sql = $sql." eliminado'".$data->eliminado."', "; 
				}
				if(!empty($data->id)){
					$sql = $sql."id='".$data->id."' "; 
				}
				echo $sql;
				$sql = $sql." WHERE id='".$data->id."'";
				if ($conn->query($sql) === TRUE) {
					$message="New record created successfully";
				} else {
					$message="Error: " . $sql . ", " . $conn->error;
				}
				$conn->close();
				echo json_encode(array(
					"method:" => $met,
					"message" => $message,
					"data-receive" => array(
					  "id" => $data->id
					)
				  )
				);
			}				
			else{
				http_response_code(503);
				echo json_encode(array("message" => "No data was received"));
			}
		
		break;
		
		case 'DELETE':
			if(!empty($data->id)){
				http_response_code(201);
				$conn = connect();
				$sql = $sql = "UPDATE books SET eliminado=1 WHERE id='".$data->id."'";
				if ($conn->query($sql) === TRUE) {
					$message="New record created successfully";
				} else {
					$message="Error: " . $sql . ", " . $conn->error;
				}
				$conn->close();
				echo json_encode(array(
					"method:" => $met,
					"message" => $message,
					"data-receive" => array(
					  "id" => $data->id
					)
				  )
				);
			}				
			else{
				http_response_code(503);
				echo json_encode(array("message" => "No data was received"));
			}
		break;
		case 'GET':
			$conn = connect();
			$sql = "SELECT id, name, description, price, eliminado FROM books";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					echo $row["id"]." - ".$row["name"]." - ".$row["description"]." - ".$row["price"]." - ".$row["eliminado"]."<br>";
				}
			} else {
				echo "0 results";
			}
			$conn->close();
		break;
		default:
			http_response_code(503);
			echo json_encode(array("message" => "No data was received"));
	}
	
	function connect(){
		$servername = "mysql1004.mochahost.com";
		$username = "nsierrar_sd2020";
		$password = "n513rr4r5d";
		$dbname = "nsierrar_sd2020";
		
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		echo "Connected successfully<br>";

		echo "Books' Records<br>";
		return $conn; 
	}

?>
