package com.upiita.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdBooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdBooksApplication.class, args);
	}

}
