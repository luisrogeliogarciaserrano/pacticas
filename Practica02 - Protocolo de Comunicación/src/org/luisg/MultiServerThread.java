package org.upiita;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MultiServerThread {
    private Socket socket = null;

    public MultiServerThread(Socket socket) {
        //super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient.NoClients++;
    }



    public void run() {

        try {
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;

            while((lineIn = entrada.readLine()) != null){
                System.out.println("Received: "+lineIn);
                escritor.flush();
                if(lineIn.equals("FIN")){
                    ServerMultiClient.NoClients--;
                    break;
                }else{
                    if(lineIn.substring(0,1).equals("#")){
                        String [] protocol = lineIn.split("#");

                        if(protocol.length == 4){
                            switch(protocol[1]){
                                case "mayu":
                                    if(isNumeric(protocol[2])){
                                        escritor.println("#R-may#" + protocol[2] + "#" + protocol[3].toUpperCase() + "#");
                                    }
                                    break;
                                case "min":
                                    if(isNumeric(protocol[2])){
                                        escritor.println("R-#min#" + protocol[2] + "#" + protocol[3].toLowerCase() +  "#");
                                    }
                                    break;
                                case "inv" :
                                    String cadenaInv = "";
                                    if(isNumeric(protocol[2])){
                                        for (int x=protocol[3].length()-1;x>=0;x--)
                                            cadenaInv = cadenaInv + protocol[3].charAt(x);
                                        escritor.println("#R-inv#" + protocol[2] + "#" + cadenaInv + "#");
                                    }
                                    break;
                                case "len":
                                    if(isNumeric(protocol[2])){
                                        escritor.println("#R-len#" + protocol[2] + "#" +protocol[3].length() + "#");
                                    }
                                    break;
                                case "alea" :
                                    if(isNumeric(protocol[2])){
                                        escritor.println("#R-alea#" + protocol[2] + "#" +Math.random()*Integer.valueOf(protocol[3]));
                                    }
                                    break;
                                default :
                                    escritor.println("Echo... "+lineIn);
                                    break;
                            }
                        }
                        else{
                            escritor.println("Echo... "+lineIn);
                        }
                    }
                    else{
                        escritor.println("Echo... "+lineIn);
                    }
                    //escritor.println("Echo... "+lineIn);
                    escritor.flush();
                }
            }
            try{
                entrada.close();
                escritor.close();
                socket.close();
            }catch(Exception e){
                System.out.println ("Error : " + e.toString());
                socket.close();
                System.exit (0);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
}
